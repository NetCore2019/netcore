﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NetCore.Infrastructure;
using NetCore.Models;
using NetCore.ViewModels;
using NLog;

namespace NetCore.Controllers
{
    public class ProductsController : Controller
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IConfiguration _config;
        private readonly NorthwindContext _context;

        public ProductsController(NorthwindContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        public async Task<IActionResult> Index()
        {
            var productPageSettings = _config.GetSection("ProductPageSettings");
            var maxCount = int.Parse(productPageSettings.GetSection("MaxCount").Value);
            Logger.Info($"MaxCount {maxCount}");
            var takeProductCount = maxCount == 0 ? int.MaxValue : maxCount;

            var productViewModel = await _context.Products.Take(takeProductCount).Select(p => new ProductViewModel
            {
                ProductID = p.ProductID,
                ProductName = p.ProductName,
                SupplierName = _context.Suppliers.Where(s => s.SupplierID == p.SupplierID).Select(s => s.ContactName).SingleOrDefault(),
                CategoryName = _context.Categories.Where(c => c.CategoryID == p.CategoryId).Select(c => c.CategoryName).SingleOrDefault(),
                QuantityPerUnit = p.QuantityPerUnit,
                UnitPrice = p.UnitPrice,
                UnitsInStock = p.UnitsInStock,
                UnitsOnOrder = p.UnitsOnOrder,
                ReorderLevel = p.ReorderLevel,
                Discontinued = p.Discontinued
            }).ToListAsync();

            return View(productViewModel);
        }

        public IActionResult Create()
        {
            var viewModel = new EditProductViewModel();
            var categories = _context.Categories.Select(c => c.CategoryName);
            var selectListCategories = new SelectList(categories);
            var suppliers = _context.Suppliers.Select(s => s.ContactName);
            var selectListSuppliers = new SelectList(suppliers);

            viewModel.RelatedCategories = selectListCategories;
            viewModel.RelatedSuppliers = selectListSuppliers;

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EditProductViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var product = CreateProductFromViewModel(viewModel);

                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            var viewModel = new EditProductViewModel
            {
                ProductName = product.ProductName,
                QuantityPerUnit = product.QuantityPerUnit,
                UnitPrice = product.UnitPrice,
                UnitsInStock = product.UnitsInStock,
                UnitsOnOrder = product.UnitsOnOrder,
                ReorderLevel = product.ReorderLevel,
                Discontinued = product.Discontinued
            };

            var categories = _context.Categories.Select(c => c.CategoryName);
            var selectListCategories = new SelectList(categories);
            var suppliers = _context.Suppliers.Select(s => s.ContactName);
            var selectListSuppliers = new SelectList(suppliers);

            viewModel.RelatedCategories = selectListCategories;
            viewModel.RelatedSuppliers = selectListSuppliers;

            var category = _context.Categories.SingleOrDefault(c => c.CategoryID == product.CategoryId);
            var supplier = _context.Suppliers.SingleOrDefault(s => s.SupplierID == product.SupplierID);

            if (category != null && supplier != null)
            {
                viewModel.CategoryName = category.CategoryName;
                viewModel.SupplierName = supplier.ContactName;

                var selectedCategory = selectListCategories.FirstOrDefault(c => c.Value == category.CategoryName);
                if (selectedCategory != null)
                {
                    selectedCategory.Selected = true;
                }

                var selectedSupplier = selectListSuppliers.FirstOrDefault(c => c.Value == supplier.ContactName);
                if (selectedSupplier != null)
                {
                    selectedSupplier.Selected = true;
                }
            }

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditProductViewModel viewModel)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Entry(product).State = EntityState.Detached;
                    var updatedProduct = CreateProductFromViewModel(viewModel);
                    updatedProduct.ProductID = id;
                    _context.Entry(updatedProduct).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.ProductID))
                    {
                        return NotFound();
                    }

                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        private Product CreateProductFromViewModel(EditProductViewModel viewModel)
        {
            var product = new Product
            {
                ProductName = viewModel.ProductName,
                QuantityPerUnit = viewModel.QuantityPerUnit,
                UnitPrice = viewModel.UnitPrice,
                UnitsInStock = viewModel.UnitsInStock,
                UnitsOnOrder = viewModel.UnitsOnOrder,
                ReorderLevel = viewModel.ReorderLevel,
                Discontinued = viewModel.Discontinued
            };

            var category = _context.Categories.SingleOrDefault(c => c.CategoryName == viewModel.CategoryName);
            var supplier = _context.Suppliers.SingleOrDefault(s => s.ContactName == viewModel.SupplierName);

            if (category != null && supplier != null)
            {
                product.CategoryId = category.CategoryID;
                product.SupplierID = supplier.SupplierID;
            }

            return product;
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.ProductID == id);
        }
    }
}
