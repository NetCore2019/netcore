﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NLog;

namespace NetCore.Pages
{
    public class ErrorModel : PageModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void OnGet()
        {
            var error = this.HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            Logger.Error(error.Error.Message);
        }
    }
}