﻿using Microsoft.EntityFrameworkCore;
using NetCore.Models;

namespace NetCore.Infrastructure
{
    public sealed class NorthwindContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }

        public NorthwindContext(DbContextOptions<NorthwindContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
