﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NetCore.ViewModels
{
    public class EditProductViewModel
    {
        public int ProductID { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The product name must be between 2 and 50 characters")]
        public string ProductName { get; set; }

        [Required]
        public string QuantityPerUnit { get; set; }

        [Required]
        public string SupplierName { get; set; }

        [Required]
        public string CategoryName { get; set; }

        [Required]
        [Range(1, 5000, ErrorMessage = "The UnitPrice should be between 1 and 5000")]
        public decimal UnitPrice { get; set; }

        [Required]
        public short UnitsInStock { get; set; }

        [Required]
        public short UnitsOnOrder { get; set; }

        [Required]
        public short ReorderLevel { get; set; }

        [Required]
        public bool Discontinued { get; set; }

        public SelectList RelatedSuppliers { get; set; }

        public SelectList RelatedCategories { get; set; }
    }
}
